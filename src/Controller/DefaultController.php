<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        //php speed
        $phpStart = microtime(true);
        for ($i=0; $i<10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {
                foreach ($b as $j) {
                    $c[] = $h*$j;
                }
            }
        }
        $phpEnd = microtime(true);

        //network speed
        $netwStart = microtime(true);
        for ($i=0; $i<3; $i++) {
            //TO DO: uncomment and solve this error:
            //file_get_contents(): php_network_getaddresses: getaddrinfo failed: Temporary failure in name resolution
            //$file = file_get_contents("http://www.google.com");
        }
        $netwEnd = microtime(true);

        //File system IO speed
        $fsStart = microtime(true);
        for ($i=0; $i<1000; $i++) {
            $file = fopen(__DIR__, "r");
        }
        $fsEnd = microtime(true);

        $phpTime = round($phpEnd - $phpStart,5);
        $netwTime = round($netwEnd - $netwStart,5);
        $fsTime = round($fsEnd - $fsStart,5);

        $file = getenv("DOCUMENT_ROOT") . "/../results.txt";
        $content = "\n". 'File system IO speed: '.$fsTime. "\n" .'Network speed: '.$netwTime. "\n". 'PHP execution speed: '.$phpTime."\n";
        file_put_contents($file,$content,FILE_APPEND);

        return new Response('<html><body>   File system IO speed: '.$fsTime.'<br>
                                            Network speed: '.$netwTime.'<br>
                                            PHP execution speed: '.$phpTime.'<br></body></html>');
    }
}
